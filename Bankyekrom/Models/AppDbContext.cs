﻿using System;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bankyekrom.Models
{
    public class AppDbContext : IdentityDbContext<User>
    {
        public AppDbContext() : base("DefaultConnection") { }

        public DbSet<Profile> Profiles { get; set; }
        public DbSet<ResetRequest> ResetRequests { get; set; }
        public DbSet<MessageOutboxEntry> MessageOutboxEntries { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<District> Districts { get; set; }
        public DbSet<CatchmentArea> CatchmentAreas { get; set; }
        public DbSet<Service> SeasonServices { get; set; }
        public DbSet<Variety> Varieties { get; set; }
        public DbSet<Processor> Processors { get; set; }
        public DbSet<Farmer> Farmers { get; set; }
        public DbSet<Farm> Farms { get; set; }
        public DbSet<PlantingSeason> PlantingSeasons { get; set; }
        public DbSet<ServiceRequest> ServiceRequests { get; set; }
        public DbSet<ServiceProvider> ServiceProviders { get; set; }
        public DbSet<ServiceProviderService> ServiceProviderServices { get; set; }
        public DbSet<OfflineData> OfflineData { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<AppDbContext, Migrations.Configuration>());
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Added)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.CreatedAt = DateTime.UtcNow;
                entry.ModifiedAt = DateTime.UtcNow;
            }

            foreach (var entry in ChangeTracker.Entries()
                .Where(x => x.State == EntityState.Modified)
                .Select(x => x.Entity)
                .OfType<IAuditable>())
            {
                entry.ModifiedAt = DateTime.UtcNow;
            }
            return base.SaveChanges();
        }
    }
}