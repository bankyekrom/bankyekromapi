﻿namespace Bankyekrom.Models
{
    public class Privileges
    {
        public const string CanViewDashboard = "CanViewDashboard";
        public const string CanViewReports = "CanViewReports";
        public const string CanViewSettings = "CanViewSettings";
        public const string CanManageSettings = "CanManageSettings";
        public const string CanViewAccounts = "CanViewAccounts";
        public const string CanManageAccounts = "CanManageAccounts";
        public const string IsAgent = "IsAgent";
        public const string IsFarmer = "IsFarmer";
        public const string IsAggregator = "IsAggregator";
        public const string IsProcessor = "IsProcessor";
        public const string IsCustomer = "IsCustomer";
        public const string IsUser = "IsUser";

        public const string CanViewFarmers = "CanViewFarmers";
        public const string CanManageFarmers = "CanManageFarmers";

        public const string CanViewFarms = "CanViewFarms";
        public const string CanManageFarms = "CanManageFarms";

        public const string CanViewSeasons = "CanViewSeasons";
        public const string CanManageSeasons = "CanManageSeasons";

        public const string CanViewRequests = "CanViewRequests";
        public const string CanManageRequests = "CanManageRequests";

        public const string CanViewServices = "CanViewServices";
        public const string CanManageServices = "CanManageServices";

        public const string CanViewServiceProviders = "CanViewServiceProviders";
        public const string CanManageServiceProviders = "CanManageServiceProviders";

        public const string CanViewProcessors = "CanViewProcessors";
        public const string CanManageProcessors = "CanManageProcessors";


    }

    public class GenericProperties
    {
        public const string CreatedBy = "CreatedBy";
        public const string CreatedAt = "CreatedAt";
        public const string ModifiedAt = "ModifiedAt";
        public const string ModifiedBy = "ModifiedBy";
        public const string Locked = "Locked";
    }

    public class ExceptionMessage
    {
        public const string RecordLocked = "Record is locked and can't be deleted.";
        public const string NotFound = "Record not found.";
    }

    public class DefaultKeys
    {
        public static string CurrencyFormat = "GHC##,###.00";
    }

    public class ConfigKeys
    {
        public static string EmailAccountName = "EmailAccountName";
        public static string EmailApiKey = "EmailApiKey";
        public static string EmailSender = "EmailSender";
        public static string SmsApiKey = "SmsApiKey";
        public static string SmsSender = "SmsSender";
        public static string AppTitle = "AppTitle";
        public static string Logo = "Logo";
        public static string ToolbarColour = "ToolbarColour";
    }

}