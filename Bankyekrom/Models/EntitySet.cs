﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Claims;
using System.Threading.Tasks;
using Bankyekrom.AxHelpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bankyekrom.Models
{
    public interface IHasId
    {
        [Key]
        long Id { get; set; }
    }

    public interface ISecured
    {
        bool Locked { get; set; }
        bool Hidden { get; set; }
    }

    public interface IAuditable : IHasId
    {
        [Required]
        string CreatedBy { get; set; }
        [Required]
        string ModifiedBy { get; set; }
        DateTime CreatedAt { get; set; }
        DateTime ModifiedAt { get; set; }
    }

    public class HasId : IHasId
    {
        public long Id { get; set; }
    }

    public class AuditFields : HasId, IAuditable, ISecured
    {
        [Required]
        public string CreatedBy { get; set; }
        [Required]
        public string ModifiedBy { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
    }

    public class LookUp : AuditFields
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
    }

    public class Region : LookUp
    {
        public List<District> Districts { get; set; }
    }

    public class District : LookUp
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Code { get; set; }
        public virtual Region Region { get; set; }
        public long RegionId { get; set; }
        public List<CatchmentArea> Area { get; set; }
    }

    public class CatchmentArea : LookUp
    {
        [MaxLength(512), Required, Index(IsUnique = true)]
        public string Code { get; set; }
        [MaxLength(1000)]
        public virtual District District { get; set; }
        public long DistrictId { get; set; }
    }

    public class User : IdentityUser
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        public virtual Profile Profile { get; set; }
        public long ProfileId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime ModifiedAt { get; set; }
        public bool IsDeleted { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
        public string Token { get; set; }
        public bool TokenVerified { get; set; } = false;
        [NotMapped]
        public string Password { get; set; }
        [NotMapped]
        public string ConfirmPassword { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            return userIdentity;
        }
        public virtual CatchmentArea Area { get; set; }
        public long? AreaId { get; set; }
    }

    public class Profile : HasId
    {
        [Required, MaxLength(512), Index(IsUnique = true)]
        public string Name { get; set; }
        [MaxLength(1000)]
        public string Notes { get; set; }
        [MaxLength(500000)]
        public string Privileges { get; set; }
        public bool Locked { get; set; }
        public bool Hidden { get; set; }
        public List<User> Users { get; set; }
    }
    
    public class Farmer : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(16), Required, Index(IsUnique = true)]
        public string Reference { get; set; } = MessageHelpers.GenerateRandomNumber(8);
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string ResidentialAddress { get; set; }
        public string Town { get; set; }
        public virtual CatchmentArea Area { get; set; }
        public long AreaId { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; } = true;
        public virtual List<Farm> Farms { get; set; }
        public Gender Gender { get; set; }
        public string GhanaPostGps { get; set; }
        public long EId { get; set; }
    }

    public class Farm : AuditFields
    {
        [MaxLength(128), Required, Index(IsUnique = true)]
        public string Code { get; set; } = MessageHelpers.GenerateRandomString(8).ToUpper();
        public Farmer Farmer { get; set; }
        public long FarmerId { get; set; }
        [MaxLength(128), Required]
        public string Reference { get; set; } = MessageHelpers.GenerateRandomNumber(32);
        public string Town { get; set; }
        public string Location { get; set; }
        public double NumberOfAcres { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public District District { get; set; }
        public long DistrictId { get; set; }
        public bool IsDeleted { get; set; }
        public virtual List<PlantingSeason> Seasons { get; set; }
        public virtual List<ServiceRequest> Requests { get; set; }
        public string GhanaPostGps { get; set; }
        public long EId { get; set; }
    }

    public class FarmGeoDataModel
    {
        public long Id { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
    }

    public class PlantingSeason : AuditFields
    {
        [MaxLength(128), Required]
        public string Reference { get; set; } = MessageHelpers.GenerateRandomNumber(16);
        public Farm Farm { get; set; }
        public long FarmId { get; set; }
        public bool IsActive { get; set; }
        public DateTime DatePlanted { get; set; }
        public DateTime ProjectedHarvestDate { get; set; }
        public DateTime? HarvestDate { get; set; }
        public virtual Variety Variety { get; set; }
        public long VarietyId { get; set; }
        public string Notes { get; set; }
        public double ExpectedQuantity { get; set; }
        public double ActualQuantity { get; set; }
        public long EId { get; set; }
    }

    public class ServiceRequest : AuditFields
    {
        public Farm Farm { get; set; }
        public long FarmId { get; set; }
        public DateTime ServiceDate { get; set; }
        public string Notes { get; set; }
        public virtual Service Service { get; set; }
        public long ServiceId { get; set; }
        public double Charge { get; set; }
        public ServiceRequestStatus Status { get; set; } = ServiceRequestStatus.Pending;
        public ServiceProvider AssignedTo { get; set; }
        public long? AssignedToId { get; set; }
        public long EId { get; set; }
    }

    public class ServiceProvider : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(16), Required, Index(IsUnique = true)]
        public string Reference { get; set; } = MessageHelpers.GenerateRandomNumber(8);
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public virtual District District { get; set; }
        public long DistrictId { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public bool IsDeleted { get; set; }
        public bool IsActive { get; set; } = true;
        public virtual List<ServiceProviderService> Services { get; set; }
        public string GhanaPostGps { get; set; }
        [NotMapped]
        public List<long> ServiceIds { get; set; }
    }
    public class ServiceProviderService : HasId
    {
        public ServiceProvider ServiceProvider { get; set; }
        public long ServiceProviderId { get; set; }
        public virtual Service Service { get; set; }
        public long ServiceId { get; set; }
    }

    public class Processor : AuditFields
    {
        [MaxLength(128), Required]
        public string Name { get; set; }
        [MaxLength(16), Required, Index(IsUnique = true)]
        public string Reference { get; set; } = MessageHelpers.GenerateRandomNumber(8);
        public string ContactPerson { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string Town { get; set; }
        public virtual District District { get; set; }
        public long DistrictId { get; set; }
        public string GhanaPostGps { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class Service : LookUp
    {
    }

    public class Variety : LookUp
    {
        public int HarvestPeriod { get; set; } = 6;
    } 

    public class ResetRequest : HasId
    {
        public string Ip { get; set; } = "127.0.0.1";
        public DateTime Date { get; set; } = DateTime.Now;
        public string PhoneNumber { get; set; }
        public string Token { get; set; }
        public bool IsActive { get; set; } = false;
    }

    public class ResetModel
    {
        public string Token { get; set; }
        public string Password { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class VerifyModel
    {
        public string Code { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class MessageOutboxEntry
    {
        public DateTime Created { get; set; } = DateTime.Now;
        public long Id { get; set; }
        public bool IsSent { get; set; } = false;
        public DateTime LastAttemptDate { get; set; } = DateTime.Now.AddMinutes(-1);

        [MaxLength(256)]
        public string LastAttemptMessage { get; set; }

        [Required, MaxLength(5120)]
        public string Message { get; set; }

        [MaxLength(256)]
        public string Notes { get; set; }

        [Required, MaxLength(128)]
        public string Receiver { get; set; }

        [Required, MaxLength(128)]
        public string Sender { get; set; }

        [Required, MaxLength(256)]
        public string Subject { get; set; }

        [MaxLength(512)]
        public string FilePath { get; set; }
    }

    public class Message : HasId
    {
        [MaxLength(256), Required]
        public string Recipient { get; set; }
        [MaxLength(256)]
        public string Name { get; set; }
        [MaxLength(128)]
        public string Subject { get; set; }
        [Required]
        public string Text { get; set; }
        public MessageStatus Status { get; set; }
        public MessageType Type { get; set; }
        [MaxLength(5000)]
        public string Response { get; set; }
        public DateTime TimeStamp { get; set; }
        [NotMapped]
        public string Attachment { get; set; }
    }

    public enum ServiceRequestStatus
    {
        Pending,
        Started,
        Completed,
        Cancelled,
        Accepted,
    }

    public enum MessageType
    {
        SMS,
        Email
    }

    public enum MessageStatus
    {
        Sent,
        Received,
        Failed
    }

    public enum Gender
    {
        Male,
        Female
    }

    public enum UserType
    {
        Farmer,
        Aggregator,
        Agent,
        Processor,
        Admin,
        Other
    }

    public class AgentDashboardStats
    {
        public int PendingRequests { get; set; }
        public int ActiveSeasons { get; set; }
        public int RegisteredFarms { get; set; }
        public int RegisteredFarmers { get; set; }
    }

    public class GeneralStats
    {
        public int PendingRequests { get; set; }
        public int ActiveSeasons { get; set; }
        public int RegisteredFarms { get; set; }
        public int RegisteredFarmers { get; set; }
    }

    public class YearlyStats
    {
        public FarmerStats FarmerStats { get; set; } = new FarmerStats();
        public ActiveSeasonsStats SeasonsStats { get; set; } = new ActiveSeasonsStats();
    }

    public class FarmerStats
    {
        public int NumberOfMaleFarmers { get; set; } = 0;
        public double AverageMaleAge { get; set; } = 0.0;
        public double AverageMaleLandArea { get; set; } = 0.0;
        public int NumberOfFemaleFarmers { get; set; } = 0;
        public double AverageFemaleAge { get; set; } = 0.0;
        public double AverageFemaleLandArea { get; set; } = 0.0;
    }

    public class ActiveSeasonsStats
    {
        public int TotalSeasons { get; set; } = 0;
        public double S81to100 { get; set; } = 0.0;
        public double S61to80 { get; set; } = 0.0;
        public double S41to60 { get; set; } = 0;
        public double S21to40 { get; set; } = 0.0;
        public double S0to20 { get; set; } = 0.0;
    }

    public class OfflineData : HasId
    {
        public string CreatedBy { get; set; }
        public string Data { get; set; }
        public OfflineTable Table { get; set; }
        public OfflineStatus Status { get; set; }
        public DateTime? SyncedAt { get; set; }
        public DateTime TransferedAt { get; set; } = DateTime.Now;
    }

    public enum OfflineTable
    {
        Farmers,
        Farms,
        Seasons,
        Requests
    }

    public enum OfflineStatus
    {
        Pending,
        Synced,
        Unclean
    }

    public class AgentOfflineData
    {
        public List<Farmer> Farmers { get; set; } = new List<Farmer>();
        public List<Farm> Farms { get; set; } = new List<Farm>();
        public List<PlantingSeason> Seasons { get; set; } = new List<PlantingSeason>();
        public List<ServiceRequest> Requests { get; set; } = new List<ServiceRequest>();
    }

}