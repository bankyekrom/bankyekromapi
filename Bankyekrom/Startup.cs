﻿using System;
using System.IO;
using Bankyekrom;
using Microsoft.Owin;
using Newtonsoft.Json;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Bankyekrom
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            LoadSettings();
            ConfigureAuth(app);
        }

        private static void LoadSettings()
        {
            SetupConfig.Setting = JsonConvert.DeserializeObject<Setting>
                (File.ReadAllText(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SetupConfig.json")));
        }
    }
}
