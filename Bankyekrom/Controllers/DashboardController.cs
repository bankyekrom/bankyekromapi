﻿using Bankyekrom.AxHelpers;
using Bankyekrom.Extensions;
using Bankyekrom.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/dashboard")]
    public class DashboardController : ApiController
    {
        [HttpGet]
        [Route("generalstats")]
        public ResultObj GetGeneralStats()
        {
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = new GeneralStats
                {
                    ActiveSeasons = db.PlantingSeasons.Count(x => x.IsActive),
                    PendingRequests = db.ServiceRequests.Count(
                        x =>
                            x.Status != ServiceRequestStatus.Completed),
                    RegisteredFarmers = db.Farmers.Count(x => !x.IsDeleted),
                    RegisteredFarms = db.Farms.Count(x => !x.IsDeleted)
                };

                return WebHelpers.BuildResponse(data, "Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpGet]
        [Route("yearlystats")]
        public ResultObj GetYearlyStats(int year)
        {
            try
            {
                var now = DateTime.Now;
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = new YearlyStats();
                #region Farmer Statistics
                var farmers = db.Farmers.Where(x => x.CreatedAt.Year == year && !x.IsDeleted).Include(x => x.Farms);
                var maleFarmers = farmers.Where(x => x.Gender == Gender.Male);
                var femaleFarmers = farmers.Where(x => x.Gender == Gender.Female);
                data.FarmerStats.NumberOfMaleFarmers = maleFarmers.Count();
                data.FarmerStats.NumberOfFemaleFarmers = femaleFarmers.Count();

                var maleAges = maleFarmers.Where(x => x.DateOfBirth.HasValue).ToList().Select(x => DateHelpers.CalculateAge(x.DateOfBirth.Value)).ToList();
                data.FarmerStats.AverageMaleAge = maleAges.Count() > 0 ? (maleAges.Sum() / maleAges.Count()) : 0;
                var femaleAges = femaleFarmers.Where(x => x.DateOfBirth.HasValue).ToList().Select(x => DateHelpers.CalculateAge(x.DateOfBirth.Value)).ToList();
                data.FarmerStats.AverageFemaleAge = femaleAges.Count() > 0 ? (femaleAges.Sum() / femaleAges.Count()) : 0;

                var maleIds = maleFarmers.Select(x => x.Id).ToList();
                var maleFarms = db.Farms.Where(x => maleIds.Contains(x.FarmerId) && x.NumberOfAcres > 0);
                data.FarmerStats.AverageMaleLandArea = maleFarms.Count() > 0 ? maleFarms.Sum(x => x.NumberOfAcres) / maleFarms.Count() : 0;
                var femaleIds = femaleFarmers.Select(x => x.Id).ToList();
                var femaleFarms = db.Farms.Where(x => femaleIds.Contains(x.FarmerId) && x.NumberOfAcres > 0);
                data.FarmerStats.AverageFemaleLandArea = femaleFarms.Count() > 0 ? femaleFarms.Sum(x => x.NumberOfAcres) / femaleFarms.Count() : 0;
                #endregion

                #region Matured Seasons
                var seasons = db.PlantingSeasons.Where(x => x.ProjectedHarvestDate.Year == year || x.HarvestDate.Value.Year == year);
                var totalSeasons = seasons.Count();
                data.SeasonsStats.TotalSeasons = totalSeasons;
                foreach(var season in seasons)
                {
                    var percentageMatured = 0.0;
                    if (season.HarvestDate.HasValue)
                    percentageMatured = 100;
                    else
                    {
                        if (season.ProjectedHarvestDate <= now) percentageMatured = 100;
                        else
                        {
                            var datePlanted = season.DatePlanted;
                            var harvestDate = season.ProjectedHarvestDate;
                            var diff1 = (harvestDate - datePlanted).TotalDays;
                            var diff2 = (now - datePlanted).TotalDays;
                            percentageMatured = (diff2 * 100) / diff1;
                        }
                    }

                    if (percentageMatured >= 81)
                    {
                        data.SeasonsStats.S81to100++;
                    }
                    else if (percentageMatured >= 61 && percentageMatured < 81)
                    {
                        data.SeasonsStats.S61to80++;
                    }
                    else if (percentageMatured >= 41 && percentageMatured < 61)
                    {
                        data.SeasonsStats.S41to60++;
                    }
                    else if (percentageMatured >= 21 && percentageMatured < 41)
                    {
                        data.SeasonsStats.S21to40++;
                    }
                    else
                    {
                        data.SeasonsStats.S0to20++;
                    }
                }


                #endregion


                return WebHelpers.BuildResponse(data, "Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }
    }
}
