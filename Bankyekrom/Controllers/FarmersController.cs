﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.DataAccess.Repositories;
using Bankyekrom.Extensions;
using Bankyekrom.Models;
using Microsoft.AspNet.Identity;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/farmers")]
    public class FarmersController : BaseApi<Farmer>
    {
        [HttpPost]
        [Route("adminpost")]
        public ResultObj AdminPost(Farmer record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.IsActive = true;
                Repository.Insert(record);
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Post(Farmer record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.IsActive = true;
                Repository.Insert(record);
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        [HttpPut]
        [Route("adminput")]
        public ResultObj AdminPut(Farmer record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.ModifiedAt = DateTime.Now;
                record.ModifiedBy = user.UserName;
                record.Farms = null;
                Repository.Update(record);
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Put(Farmer record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.ModifiedAt = DateTime.Now;
                record.ModifiedBy = user.UserName;
                record.Farms = null;
                Repository.Update(record);
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminquery")]
        public ResultObj AdminQuery(FarmerFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.Name).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Name,
                        x.DateOfBirth,
                        x.GhanaPostGps,
                        x.Town,
                        x.ResidentialAddress,
                        x.Reference,
                        x.PhoneNumber,
                        x.AreaId,
                        Gender = x.Gender.ToString(),
                        Farms = x.Farms.Count(),
                        Area = x.Area.Name,
                        District = x.Area.District.Name,
                        Region = x.Area.District.Region.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(FarmerFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.AreaId == user.AreaId).ToList();
                var total = raw.Count();
                var data = raw.OrderBy(x => x.Name).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Name,
                        x.DateOfBirth,
                        x.GhanaPostGps,
                        x.Town,
                        x.ResidentialAddress,
                        x.Reference,
                        x.PhoneNumber,
                        x.AreaId,
                        Gender = x.Gender.ToString(),
                        Farms = x.Farms.Count(),
                        Area = x.Area.Name,
                        District = x.Area.District.Name,
                        Region = x.Area.District.Region.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("mobilequery")]
        public ResultObj MobileQuery(FarmerFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.AreaId == user.AreaId).ToList();
                var total = raw.Count();
                var data = raw.OrderBy(x => x.Name)
                    .Take(filter.Pager.Size * filter.Pager.Page)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Name,
                        x.DateOfBirth,
                        x.GhanaPostGps,
                        x.Town,
                        x.ResidentialAddress,
                        x.Reference,
                        x.PhoneNumber,
                        x.AreaId,
                        Gender = x.Gender.ToString(),
                        Farms = x.Farms.Count(),
                        Area = x.Area.Name,
                        District = x.Area.District.Name,
                        Region = x.Area.District.Region.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        [HttpGet]
        [Route("adminget")]
        public ResultObj AdminGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.Farmers.Where(x => x.Id > 0 && !x.IsDeleted).Include(x => x.Area.District.Region).OrderBy(x => x.Reference).ToList().Select(x =>
                      new
                      {
                          x.CreatedAt,
                          x.CreatedBy,
                          x.ModifiedAt,
                          x.ModifiedBy,
                          x.Id,
                          x.Name,
                          x.DateOfBirth,
                          x.GhanaPostGps,
                          x.Town,
                          x.ResidentialAddress,
                          x.Reference,
                          x.PhoneNumber,
                          x.AreaId,
                          Gender = x.Gender.ToString(),
                          Farms = x.Farms.Count(),
                          Area = x.Area.Name,
                          District = x.Area.District.Name,
                          Region = x.Area.District.Region.Name
                      }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Farmers.Where(x => x.AreaId == user.AreaId && !x.IsDeleted).Include(x => x.Area.District.Region).OrderBy(x => x.Name).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Name,
                           x.DateOfBirth,
                           x.GhanaPostGps,
                           x.Town,
                           x.ResidentialAddress,
                           x.Reference,
                           x.PhoneNumber,
                           x.AreaId,
                           Gender = x.Gender.ToString(),
                           Farms = x.Farms.Count(),
                           Area = x.Area.Name,
                           District = x.Area.District.Name,
                           Region = x.Area.District.Region.Name
                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.Farmers.Where(x => x.Id == id).Include(x => x.Area.District.Region).Include(x => x.Farms).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                        new
                        {
                            x.CreatedAt,
                            x.CreatedBy,
                            x.ModifiedAt,
                            x.ModifiedBy,
                            x.Id,
                            x.Name,
                            x.DateOfBirth,
                            x.GhanaPostGps,
                            x.Town,
                            x.ResidentialAddress,
                            x.Reference,
                            x.PhoneNumber,
                            x.AreaId,
                            Gender = x.Gender.ToString(),
                            Area = x.Area.Name,
                            District = x.Area.District.Name,
                            Region = x.Area.District.Region.Name,
                            Farms = x.Farms.Select(f => new
                            {
                                f.Id,
                                f.Code,
                                f.Location,
                                f.Latitude,
                                f.Longitude,
                                f.Town,
                                f.NumberOfAcres
                            }).ToList()
                        }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        //[HttpGet]
        //[Route("getdistrictfarmers")]
        //public ResultObj GetDistictFarmers(long districtId)
        //{
        //    ResultObj results;
        //    try
        //    {
        //        var user = User.Identity.AsAppUser().Result;
        //        var db = new AppDbContext();
        //        var data = db.Farmers.Where(x => x.CreatedBy == user.UserName && !x.IsDeleted).Include(x => x.District.Region).Include(x => x.Farms).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
        //                new
        //                {
        //                    x.Id,
        //                    x.Name,
        //                    x.DateOfBirth,
        //                    x.Town,
        //                    x.PhoneNumber,
        //                    District = x.District?.Name,
        //                    Farms = x.Farms?.Select(f => new
        //                    {
        //                        f.Id,
        //                        f.Reference,
        //                        f.Code,
        //                        f.Location,
        //                        f.Latitude,
        //                        f.Longitude,
        //                        f.Town,
        //                        f.NumberOfAcres,
        //                        District = f.District?.Name
        //                    }).ToList()
        //                }).ToList();
        //        results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
        //    }
        //    catch (Exception ex)
        //    {
        //        results = WebHelpers.ProcessException(ex);
        //    }
        //    return results;
        //}

        [HttpDelete]
        [Route("admindelete")]
        public ResultObj AdminDelete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Farmers.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                rec.IsDeleted = true;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Farmers.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.Farmers.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("agentget")]
        public ResultObj AgentGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var raw = db.Farmers.Where(x => x.AreaId == user.AreaId).ToList();
                var total = raw.Count();
                var data = raw.OrderBy(x => x.Name).ToList()
                    .Select(x => new
                    {
                        SyncedAt = DateTime.Now,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        EId = x.Id,
                        x.Name,
                        x.DateOfBirth,
                        x.GhanaPostGps,
                        x.Town,
                        x.ResidentialAddress,
                        x.Reference,
                        x.PhoneNumber,
                        x.AreaId,
                        Gender = x.Gender.ToString(),
                        Farms = x.Farms.Count(),
                        Area = x.Area.Name,
                        District = x.Area.District.Name,
                        Region = x.Area.District.Region.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}
