﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Repositories;
using Bankyekrom.Extensions;
using Bankyekrom.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security;
using WebGrease.Css.Extensions;

namespace Bankyekrom.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/agents")]
    public class AgentsController : ApiController
    {
        private readonly UserRepository _userRepo = new UserRepository();
        [HttpPost]
        [Route("login")]
        public ResultObj Login(AgentLoginModel model)
        {
            try
            {
                var userMan = new UserManager<User>(new UserStore<User>(new AppDbContext()));
                if (!ModelState.IsValid) throw new Exception("Please check the login details");

                var user = userMan.Find(model.Username, model.Password);

                if (user == null) throw new Exception("Invalid Username or Password");

                var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
                authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                var identity = userMan.CreateIdentity(user, DefaultAuthenticationTypes.ApplicationCookie);
                authenticationManager.SignIn(new AuthenticationProperties { IsPersistent = true }, identity);

                var ticket = new AuthenticationTicket(identity, new AuthenticationProperties());
                var token = Startup.OAuthOptions.AccessTokenFormat.Protect(ticket);

                var data = new
                {
                    user.Id,
                    Username = user.UserName,
                    user.Name,
                    user.CreatedAt,
                    user.ModifiedAt,
                    Role = new
                    {
                        user.Profile.Id,
                        user.Profile.Name,
                        Privileges = user.Profile.Privileges.Split(',')
                    },
                    Token = token,
                };

                return WebHelpers.BuildResponse(data, "Login Successful", true, 0);
            }
            catch (Exception e)
            {
                return WebHelpers.ProcessException(e);
            }
        }

        [HttpGet]
        [Route("logout")]
        [Authorize]
        public ResultObj Logout()
        {
            var authenticationManager = HttpContext.Current.GetOwinContext().Authentication;
            authenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return WebHelpers.BuildResponse(new { }, "Logged Out Successfully", true, 0);
        }

        [Authorize]
        [Route("changepassword")]
        public ResultObj ChangePassword(ChangePasswordBindingModel model)
        {
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var userMan = new UserManager<User>(new UserStore<User>(new AppDbContext()));
                if (!ModelState.IsValid) return WebHelpers.ProcessException(ModelState.Values);

                var result = userMan.ChangePassword(user.Id,
                    model.OldPassword, model.NewPassword);

                return !result.Succeeded ? WebHelpers.ProcessException(result)
                    : WebHelpers.BuildResponse(model, "Password changed sucessfully.", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }

        }

        [Route("getagents")]
        public ResultObj GetAgents()
        {
            try
            {
                var db = new AppDbContext();
                var data = db.Users.Include(x => x.Profile).Where(x => !x.IsDeleted && x.Profile.Name.ToLower().Contains("agent")).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.PhoneNumber,
                        Username = x.UserName,
                        x.CreatedAt,
                        RoleId = x.ProfileId,
                        x.ProfileId,
                        Role = new { x.Profile.Id, x.Profile.Name },
                        Profile = new
                        {
                            x.Profile.Id,
                            x.Profile.Name
                        },
                        Area = x.Area?.Name,
                        x.AreaId
                    }).ToList();
                return WebHelpers.BuildResponse(data, "Loaded Successfully", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }
        [HttpGet]
        [Route("getagentdetails")]
        public ResultObj GetAgentDetails(string id)
        {
            try
            {
                var db = new AppDbContext();
                var data = db.Users.Where(x => x.Id == id && x.Profile.Name.ToLower().Contains("agent")).ToList()
                    .Select(x => new
                    {
                        x.Id,
                        x.Name,
                        x.PhoneNumber,
                        Username = x.UserName,
                        x.CreatedAt,
                        RoleId = x.ProfileId,
                        x.ProfileId,
                        Role = new { x.Profile.Id, x.Profile.Name },
                        Profile = new
                        {
                            x.Profile.Id,
                            x.Profile.Name
                        },
                        Area = x.Area?.Name,
                        x.AreaId
                    }).FirstOrDefault();
                return WebHelpers.BuildResponse(data, "Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpPost]
        [Route("updateagentprofile")]
        public ResultObj UpdateAgentProfile(User model)
        {
            ResultObj results;
            try
            {
                var usr = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();

                var user = db.Users.First(x => x.Id == usr.Id);
                user.Name = model.Name;
                user.Email = model.Email;
                user.PhoneNumber = model.PhoneNumber;
                user.ModifiedAt = DateTime.Now;
                user.AreaId = model.AreaId;
                db.SaveChanges();
                db.SaveChanges();
                results = WebHelpers.BuildResponse(null, "Agent Profile Updated Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        [HttpDelete]
        [Route("deleteagent")]
        public ResultObj DeleteAgent(string id)
        {
            ResultObj results;
            try
            {
                _userRepo.Delete(id);
                results = WebHelpers.BuildResponse(id, "User Deleted Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        [HttpGet]
        [Route("restoreagent")]
        public ResultObj RestoreAgent(string id)
        {
            ResultObj results;
            try
            {
                _userRepo.Restore(id);
                results = WebHelpers.BuildResponse(id, "User Restored Successfully.", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }

            return results;
        }

        // POST api/Account/Register
        [Route("CreateAgent")]
        public async Task<ResultObj> CreateAgent(User model)
        {
            try
            {
                var db = new AppDbContext();
                var userMan = new UserManager<User>(new UserStore<User>(new AppDbContext()));
                if (!ModelState.IsValid) return WebHelpers.ProcessException(ModelState.Values);
                var role = db.Profiles.FirstOrDefault(x => x.Name.ToLower().Contains("agent"));
                if (role == null) throw new Exception("System Error");

                var token = MessageHelpers.GenerateRandomNumber(6);

                var user = new User
                {
                    UserName = model.UserName,
                    PhoneNumber = model.PhoneNumber,
                    ProfileId = role.Id,
                    AreaId = model.AreaId,
                    Name = model.Name,
                    Email = model.Email,
                    CreatedAt = DateTime.UtcNow,
                    ModifiedAt = DateTime.UtcNow,
                    Token = token,
                    TokenVerified = true
                };

                var identityResult = await userMan.CreateAsync(user, model.Password);
                if (!identityResult.Succeeded) return WebHelpers.ProcessException(identityResult);

                //Add Roles in selected Role to user
                if (!string.IsNullOrEmpty(role.Privileges))
                {
                    role.Privileges.Split(',').ForEach(r => userMan.AddToRole(user.Id, r.Trim()));
                }
                db.SaveChanges();

                return WebHelpers.BuildResponse(user, "Agent Created Successfully", true, 1);

            }
            catch (Exception ex)
            {
                return WebHelpers.ProcessException(ex);
            }
        }

        [HttpPost]
        [Route("saveofflinedata")]
        public ResultObj SaveOfflineData(AgentOfflineData data)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var usr = User.Identity.AsAppUser().Result;

                        new AgentsRepo().SaveOrUpdateFarmers(data.Farmers, db, usr);
                        new AgentsRepo().SaveOrUpdateFarms(data.Farms, db, usr);
                        new AgentsRepo().SaveOrUpdateSeasons(data.Seasons, db, usr);
                        new AgentsRepo().SaveOrUpdateRequests(data.Requests, db, usr);

                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(null, "Data has been synced Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }
    }
}
