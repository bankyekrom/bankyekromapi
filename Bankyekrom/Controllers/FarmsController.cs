﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/farms")]
    public class FarmsController : BaseApi<Farm>
    {

        public override ResultObj Post(Farm record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.Farmer = null;
                Repository.Insert(record);
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Put(Farm record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var rec = db.Farms.Where(x => x.Id == record.Id).FirstOrDefault();
                if (rec == null) throw new Exception("Update was not successful");
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;
                rec.FarmerId = record.FarmerId;
                rec.Town = record.Town;
                rec.Location = record.Location;
                rec.NumberOfAcres = record.NumberOfAcres;
                rec.Latitude = record.Latitude;
                rec.Longitude = record.Longitude;
                rec.GhanaPostGps = record.GhanaPostGps;
                rec.DistrictId = record.DistrictId;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(FarmsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.Farmer.AreaId == user.AreaId).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Code,
                        x.Town,
                        x.IsDeleted,
                        x.FarmerId,
                        Farmer = x.Farmer.Name,
                        x.Farmer.PhoneNumber,
                        x.NumberOfAcres,
                        x.Longitude,
                        x.Latitude,
                        x.Reference,
                        x.Location,
                        x.GhanaPostGps,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Requests = x.Requests.Count(),
                        Seasons = x.Seasons.Count(),
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("mobilequery")]
        public ResultObj MobileQuery(FarmsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.Farmer.AreaId == user.AreaId).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Code,
                        x.Town,
                        x.IsDeleted,
                        x.FarmerId,
                        Farmer = x.Farmer.Name,
                        x.Farmer.PhoneNumber,
                        x.NumberOfAcres,
                        x.Longitude,
                        x.Latitude,
                        x.Reference,
                        x.Location,
                        x.GhanaPostGps,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Requests = x.Requests.Count(),
                        Seasons = x.Seasons.Count()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("agentget")]
        public ResultObj AgentGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var raw = db.Farms.Where(x => x.Farmer.AreaId == user.AreaId).Include(x=> x.Farmer.Area).Include(x=> x.District.Region).ToList();
                var total = raw.Count();
                var data = raw.OrderBy(x => x.Code).ToList()
                    .Select(x => new
                    {
                        SyncedAt = DateTime.Now,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Code,
                        x.Town,
                        x.IsDeleted,
                        x.FarmerId,
                        Farmer = x.Farmer.Name,
                        x.Farmer.PhoneNumber,
                        x.NumberOfAcres,
                        x.Longitude,
                        x.Latitude,
                        x.Reference,
                        x.Location,
                        x.GhanaPostGps,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Requests = x.Requests.Count(),
                        Seasons = x.Seasons.Count()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Farms.Where(x => !x.IsDeleted).Include(x => x.Farmer.Area.District.Region).Include(x => x.Farmer).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                        new
                        {
                            x.CreatedAt,
                            x.CreatedBy,
                            x.ModifiedAt,
                            x.ModifiedBy,
                            x.Id,
                            EId = x.Id,
                            x.Code,
                            x.Town,
                            x.IsDeleted,
                            x.FarmerId,
                            Farmer = x.Farmer.Name,
                            x.Farmer.PhoneNumber,
                            x.NumberOfAcres,
                            x.Longitude,
                            x.Latitude,
                            x.Reference,
                            x.Location,
                            x.GhanaPostGps,
                            x.DistrictId,
                            District = x.District.Name,
                            Region = x.District.Region.Name,
                            Requests = x.Requests.Count(),
                            Seasons = x.Seasons.Count()
                        }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Farms.Where(x => x.Id == id).Include(x => x.Farmer.Area.District.Region).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Code,
                           x.Town,
                           x.IsDeleted,
                           x.FarmerId,
                           Farmer = x.Farmer.Name,
                           x.Farmer.PhoneNumber,
                           x.NumberOfAcres,
                           x.Longitude,
                           x.Latitude,
                           x.Reference,
                           x.Location,
                           x.GhanaPostGps,
                           x.DistrictId,
                           District = x.District.Name,
                           Region = x.District.Region.Name,
                           Requests = x.Requests.Count(),
                           Seasons = x.Seasons?.Select(f => new
                           {
                               f.Id,
                               f.DatePlanted,
                               f.HarvestDate,
                               f.ProjectedHarvestDate,
                               f.Notes,
                               f.VarietyId,
                               Variety = f.Variety.Name,
                               f.IsActive
                           })
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Farms.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.Farms.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("setgeodata")]
        public ResultObj SetGeoData(FarmGeoDataModel obj)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var rec = db.Farms.FirstOrDefault(x => x.Id == obj.Id);
                if (rec == null) throw new Exception("Please check the selected farm");
                rec.Latitude = obj.Latitude;
                rec.Longitude = obj.Longitude;
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(obj.Id, "Location Set Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminpost")]
        public ResultObj AdminPost(Farm record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.Farmer = null;
                Repository.Insert(record);
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPut]
        [Route("adminput")]
        public ResultObj AdminPut(Farm record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var rec = db.Farms.Where(x => x.Id == record.Id).FirstOrDefault();
                if (rec == null) throw new Exception("Update was not successful");
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;
                rec.FarmerId = record.FarmerId;
                rec.Town = record.Town;
                rec.Location = record.Location;
                rec.NumberOfAcres = record.NumberOfAcres;
                rec.Latitude = record.Latitude;
                rec.Longitude = record.Longitude;
                rec.GhanaPostGps = record.GhanaPostGps;
                rec.DistrictId = record.DistrictId;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminquery")]
        public ResultObj AdminQuery(FarmsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.CreatedAt).ThenBy(x => x.Town).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Code,
                        x.Town,
                        x.IsDeleted,
                        x.FarmerId,
                        Farmer = x.Farmer.Name,
                        x.Farmer.PhoneNumber,
                        x.NumberOfAcres,
                        x.Longitude,
                        x.Latitude,
                        x.Reference,
                        x.Location,
                        x.GhanaPostGps,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Requests = x.Requests.Count(),
                        Seasons = x.Seasons.Count()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminmapquery")]
        public ResultObj AdminMapQuery(FarmsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x=> x.Latitude != 0 && x.Longitude != 0);
                var data = raw.OrderBy(x => x.CreatedAt).ThenBy(x => x.Town)
                    .Take(filter.Size)
                    .ToList()
                    .Select(x => new
                    {                        
                        x.Id,
                        x.Code,
                        x.Town,
                        Farmer = x.Farmer.Name,
                        x.Farmer.PhoneNumber,
                        x.NumberOfAcres,
                        x.Longitude,
                        x.Latitude,
                        x.Reference,
                        x.Location,
                        x.GhanaPostGps,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        Label = x.Code+" ("+x.Town+")"
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("adminget")]
        public ResultObj AdminGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.Farms.Where(x => x.Id > 0 && !x.IsDeleted).Include(x => x.Farmer.Area.District.Region).Include(x => x.District.Region).OrderBy(x => x.Reference).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Code,
                           x.Town,
                           x.IsDeleted,
                           x.FarmerId,
                           Farmer = x.Farmer?.Name,
                           x.NumberOfAcres,
                           x.Longitude,
                           x.Latitude,
                           x.Reference,
                           x.Location,
                           x.GhanaPostGps,
                           x.DistrictId,
                           District = x.District.Name,
                           Region = x.District.Region.Name
                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpDelete]
        [Route("admindelete")]
        public ResultObj AdminDelete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Farms.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                rec.IsDeleted = true;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("getfarmerfarms")]
        public ResultObj GetFarmerFarms(long farmerId)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Farms.Where(x => x.FarmerId == farmerId).Include(x => x.Farmer.Area.District.Region).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Code,
                           x.Town,
                           x.IsDeleted,
                           x.FarmerId,
                           Farmer = x.Farmer.Name,
                           x.Farmer.PhoneNumber,
                           x.NumberOfAcres,
                           x.Longitude,
                           x.Latitude,
                           x.Reference,
                           x.Location,
                           x.GhanaPostGps,
                           x.DistrictId,
                           District = x.District.Name,
                           Region = x.District.Region.Name,
                           Requests = x.Requests.Count(),
                           Seasons = x.Seasons.Count()
                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}