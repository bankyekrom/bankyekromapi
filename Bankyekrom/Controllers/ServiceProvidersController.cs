﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/serviceproviders")]
    public class ServiceProvidersController : BaseApi<ServiceProvider>
    {
        public override ResultObj Post(ServiceProvider record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        record.CreatedAt = DateTime.Now;
                        record.ModifiedAt = DateTime.Now;
                        record.CreatedBy = user.UserName;
                        record.ModifiedBy = user.UserName;
                        record.District = null;
                        record.Services = null;
                        var serviceIds = record.ServiceIds;
                        db.ServiceProviders.Add(record);
                        db.SaveChanges();

                        foreach (var id in serviceIds)
                        {
                            db.ServiceProviderServices.Add(new ServiceProviderService
                            {
                                ServiceProviderId = record.Id,
                                ServiceId = id
                            });
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        public override ResultObj Put(ServiceProvider record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var prov = db.ServiceProviders.FirstOrDefault(x => x.Id == record.Id);

                        prov.ModifiedAt = DateTime.Now;
                        prov.ModifiedBy = user.UserName;
                        prov.Name = record.Name;
                        prov.PhoneNumber = record.PhoneNumber;
                        prov.Email = record.Email;
                        prov.ContactPerson = record.ContactPerson;
                        prov.Address = record.Address;
                        prov.Town = record.Town;
                        prov.DistrictId = record.DistrictId;
                        prov.Longitude = record.Longitude;
                        prov.Latitude = record.Latitude;
                        prov.GhanaPostGps = record.GhanaPostGps;
                        var serviceIds = record.ServiceIds;
                        db.SaveChanges();
                        var exst = db.ServiceProviderServices.Where(x => x.ServiceProviderId == record.Id);
                        db.ServiceProviderServices.RemoveRange(exst);
                        db.SaveChanges();
                        foreach (var id in serviceIds)
                        {
                            db.ServiceProviderServices.Add(new ServiceProviderService
                            {
                                ServiceProviderId = record.Id,
                                ServiceId = id
                            });
                        }
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(ServiceProviderFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Name,
                        x.ContactPerson,
                        x.Reference,
                        x.PhoneNumber,
                        x.Email,
                        x.Address,
                        x.Town,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        x.Latitude,
                        x.Longitude,
                        x.GhanaPostGps,
                        x.IsActive,
                        x.IsDeleted,
                        Services = x.Services.Count()
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.ServiceProviders.Include(x => x.District.Region).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                        new
                        {
                            x.CreatedAt,
                            x.CreatedBy,
                            x.ModifiedAt,
                            x.ModifiedBy,
                            x.Id,
                            x.Name,
                            x.ContactPerson,
                            x.Reference,
                            x.PhoneNumber,
                            x.Email,
                            x.Address,
                            x.Town,
                            x.DistrictId,
                            District = x.District.Name,
                            Region = x.District.Region.Name,
                            x.Latitude,
                            x.Longitude,
                            x.GhanaPostGps,
                            x.IsActive,
                            x.IsDeleted,
                            Services = x.Services.Count()
                        }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.ServiceProviders.Where(x => x.Id == id).Include(x => x.District.Region).Include(x => x.Services).OrderBy(x => x.Name).ToList().Select(x =>
                         new
                         {
                             x.CreatedAt,
                             x.CreatedBy,
                             x.ModifiedAt,
                             x.ModifiedBy,
                             x.Id,
                             x.Name,
                             x.ContactPerson,
                             x.Reference,
                             x.PhoneNumber,
                             x.Email,
                             x.Address,
                             x.Town,
                             x.DistrictId,
                             District = x.District.Name,
                             Region = x.District.Region.Name,
                             x.Latitude,
                             x.Longitude,
                             x.GhanaPostGps,
                             x.IsActive,
                             x.IsDeleted,
                             ServiceIds = x.Services.Select(s => s.ServiceId),
                             Services = x.Services.Select(s => new
                             {
                                 s.Id,
                                 s.ServiceId,
                                 Service = s.Service.Name
                             }).ToList()
                         }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.ServiceProviders.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.ServiceProviders.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}