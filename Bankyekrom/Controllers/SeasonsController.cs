﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.DataAccess.Repositories;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/seasons")]
    public class SeasonsController : BaseApi<PlantingSeason>
    {

        public override ResultObj Post(PlantingSeason record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var exisiting = db.PlantingSeasons.Where(x => x.IsActive && x.FarmId == record.FarmId);
                foreach (var s in exisiting)
                {
                    s.IsActive = false;
                }
                db.SaveChanges();
                var variety = db.Varieties.FirstOrDefault(x => x.Id == record.VarietyId);
                if (variety == null) throw new Exception("Invalid Variety");
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.CreatedBy = user.UserName;
                record.IsActive = true;
                record.Variety = null;
                record.ProjectedHarvestDate = record.DatePlanted.AddMonths(variety.HarvestPeriod);
                db.PlantingSeasons.Add(record);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Put(PlantingSeason record)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                results = WebHelpers.BuildResponse(null, "Not Implemented", false, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(SeasonFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                filter.CreatedBy = user.UserName;
                var raw = Repository.Query(filter).Where(x => x.CreatedBy == user.UserName && x.IsActive).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.IsActive,
                        x.DatePlanted,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer.Name,
                        x.Farm.Farmer.PhoneNumber,
                        x.FarmId,
                        Farm = x.Farm?.Code,
                        x.ProjectedHarvestDate,
                        x.HarvestDate,
                        x.Farm?.Longitude,
                        x.Farm?.Latitude,
                        x.Farm?.Location,
                        x.Farm?.Town,
                        x.Farm?.NumberOfAcres,
                        x.ActualQuantity,
                        x.ExpectedQuantity,
                        x.Reference
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("mobilequery")]
        public ResultObj MobileQuery(SeasonFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                filter.CreatedBy = user.UserName;
                var raw = Repository.Query(filter).Where(x => x.CreatedBy == user.UserName && x.IsActive).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt)
                    .Take(filter.Pager.Size * filter.Pager.Page)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.IsActive,
                        x.DatePlanted,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer.Name,
                        x.Farm.Farmer.PhoneNumber,
                        x.FarmId,
                        Farm = x.Farm?.Code,
                        x.ProjectedHarvestDate,
                        x.HarvestDate,
                        x.Farm?.Longitude,
                        x.Farm?.Latitude,
                        x.Farm?.Location,
                        x.Farm?.Town,
                        x.Farm?.NumberOfAcres,
                        x.Reference,
                        x.VarietyId,
                        Variety = x.Variety.Name,
                        x.ActualQuantity,
                        x.ExpectedQuantity,
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.PlantingSeasons.Where(x => x.CreatedBy == user.UserName && x.IsActive).Include(x => x.Farm.Farmer.Area.District.Region).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.IsActive,
                           x.DatePlanted,
                           x.FarmId,
                           x.Farm,
                           x.ProjectedHarvestDate,
                           x.HarvestDate,
                           x.Reference,
                           x.VarietyId,
                           x.ActualQuantity,
                           x.ExpectedQuantity,
                           x.Variety
                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.PlantingSeasons.Where(x => x.Id == id).Include(x => x.Farm.Farmer).Include(x => x.Farm.District.Region).Include(x => x.Variety).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.IsActive,
                           x.DatePlanted,
                           x.Farm.FarmerId,
                           Farmer = x.Farm.Farmer.Name,
                           x.Farm.Farmer.PhoneNumber,
                           x.FarmId,
                           FarmCode = x.Farm.Code,
                           x.ProjectedHarvestDate,
                           x.HarvestDate,
                           x.Farm.Longitude,
                           x.Farm.Latitude,
                           x.Farm.Location,
                           x.Farm.Town,
                           District = x.Farm.District.Name,
                           Region = x.Farm.District.Region.Name,
                           x.Reference,
                           x.Farm.NumberOfAcres,
                           x.VarietyId,
                           x.ActualQuantity,
                           x.ExpectedQuantity,
                           Variety = x.Variety.Name
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.PlantingSeasons.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.PlantingSeasons.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        [HttpGet]
        [Route("deactivate")]
        public ResultObj Deactivate(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.PlantingSeasons.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                rec.IsActive = false;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deactivated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("activate")]
        public ResultObj Activate(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var exisiting = db.PlantingSeasons.Where(x => x.Id == id);
                foreach (var s in exisiting)
                {
                    s.IsActive = false;
                }
                db.SaveChanges();
                var rec = db.PlantingSeasons.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                rec.IsActive = true;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deactivated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminpost")]
        public ResultObj AdminPost(PlantingSeason record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var exisiting = db.PlantingSeasons.Where(x => x.IsActive && x.FarmId == record.FarmId);
                foreach (var s in exisiting)
                {
                    s.IsActive = false;
                }
                db.SaveChanges();
                var variety = db.Varieties.FirstOrDefault(x => x.Id == record.VarietyId);
                if (variety == null) throw new Exception("Invalid Variety");
                var farm = db.Farms.FirstOrDefault(x => x.Id == record.FarmId);
                if (farm == null) throw new Exception("Farm not valid");
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.CreatedBy = user.UserName;
                record.IsActive = true;
                record.Variety = null;
                record.ProjectedHarvestDate = record.DatePlanted.AddMonths(variety.HarvestPeriod);
                record.ExpectedQuantity = 50 * farm.NumberOfAcres;
                db.PlantingSeasons.Add(record);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPut]
        [Route("adminput")]
        public ResultObj AdminPut(PlantingSeason record)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(record.Id, "Not Implemented", false, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminrecordharvest")]
        public ResultObj AdminRecordHarvest(PlantingSeason record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var exisiting = db.PlantingSeasons.FirstOrDefault(x => x.IsActive && x.Id == record.Id);
                if (exisiting == null) throw new Exception("Please check the planting season");

                exisiting.ModifiedAt = DateTime.Now;
                exisiting.ModifiedBy = user.UserName;
                exisiting.IsActive = false;
                exisiting.HarvestDate = record.HarvestDate;
                exisiting.ActualQuantity = record.ActualQuantity;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Harvest Recorded Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminquery")]
        public ResultObj AdminQuery(SeasonFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.DatePlanted).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.IsActive,
                        x.DatePlanted,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer.Name,
                        x.Farm.Farmer.PhoneNumber,
                        x.FarmId,
                        FarmCode = x.Farm.Code,
                        x.ProjectedHarvestDate,
                        x.HarvestDate,
                        x.Farm.Longitude,
                        x.Farm.Latitude,
                        x.Farm.Location,
                        x.Farm.Town,
                        District = x.Farm.District.Name,
                        Region = x.Farm.District.Region.Name,
                        x.Reference,
                        x.Farm.NumberOfAcres,
                        x.VarietyId,
                        x.ActualQuantity,
                        x.ExpectedQuantity,
                        Variety = x.Variety.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("queryactivemap")]
        public ResultObj AdminQueryMap(SeasonMapFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();

                var raw = db.PlantingSeasons.Where(x => x.IsActive).Include(x => x.Farm.District.Region).Include(x => x.Farm.Farmer).Include(x => x.Variety);
                if (filter.RegionId > 0) raw = raw.Where(x => x.Farm.District.RegionId == filter.RegionId);
                if (filter.DistrictId > 0) raw = raw.Where(x => x.Farm.DistrictId == filter.DistrictId);
                if (filter.VarietyId > 0) raw = raw.Where(x => x.VarietyId == filter.VarietyId);
                if (filter.FarmId > 0) raw = raw.Where(x => x.FarmId == filter.FarmId);
                if (filter.FarmerId > 0) raw = raw.Where(x => x.Farm.FarmerId == filter.FarmerId);
                if (!string.IsNullOrEmpty(filter.FarmCode)) raw = raw.Where(q => q.Farm.Code.ToLower().Contains(filter.FarmCode.ToLower()));
                var data = raw.OrderBy(x => x.ProjectedHarvestDate)
                                    .Take(filter.Size)
                                    .ToList()
                                    .Select(x => new
                                    {
                                        x.CreatedAt,
                                        x.CreatedBy,
                                        x.ModifiedAt,
                                        x.ModifiedBy,
                                        x.Id,
                                        x.Notes,
                                        x.IsActive,
                                        x.DatePlanted,
                                        x.Farm.FarmerId,
                                        Farmer = x.Farm.Farmer.Name,
                                        x.Farm.Farmer.PhoneNumber,
                                        x.FarmId,
                                        FarmCode = x.Farm.Code,
                                        x.ProjectedHarvestDate,
                                        x.HarvestDate,
                                        x.Farm.Longitude,
                                        x.Farm.Latitude,
                                        x.Farm.Location,
                                        x.Farm.Town,
                                        District = x.Farm.District.Name,
                                        Region = x.Farm.District.Region.Name,
                                        x.Reference,
                                        x.Farm.NumberOfAcres,
                                        x.VarietyId,
                                        x.ActualQuantity,
                                        x.ExpectedQuantity,
                                        Variety = x.Variety.Name
                                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        [HttpGet]
        [Route("adminget")]
        public ResultObj AdminGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.PlantingSeasons.Where(x => x.Id > 0).OrderByDescending(x => x.IsActive).ThenBy(x => x.DatePlanted).Include(x => x.Farm.Farmer).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.IsActive,
                           x.DatePlanted,
                           x.Farm.FarmerId,
                           Farmer = x.Farm.Farmer.Name,
                           x.Farm.Farmer.PhoneNumber,
                           x.FarmId,
                           Farm = x.Farm?.Code,
                           x.ProjectedHarvestDate,
                           x.HarvestDate,
                           x.Farm?.Longitude,
                           x.Farm?.Latitude,
                           x.Farm?.Location,
                           x.Farm?.Town,
                           x.Reference,
                           x.Farm?.NumberOfAcres,
                           x.VarietyId,
                           x.ActualQuantity,
                           x.ExpectedQuantity,
                           Variety = x.Variety.Name

                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("agentget")]
        public ResultObj AgentGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var raw = db.PlantingSeasons.Where(x => x.Farm.Farmer.AreaId == user.AreaId && x.IsActive).Include(x => x.Farm.Farmer.Area).Include(x=> x.Farm.District.Region).Include(x => x.Variety).ToList();
                var total = raw.Count();
                var data = raw.OrderBy(x => x.ProjectedHarvestDate).ToList()
                    .Select(x => new
                    {
                        SyncedAt = DateTime.Now,
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        EId = x.Id,
                        x.Notes,
                        x.IsActive,
                        x.DatePlanted,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer.Name,
                        x.Farm.Farmer.PhoneNumber,
                        x.FarmId,
                        FarmCode = x.Farm?.Code,
                        x.ProjectedHarvestDate,
                        x.HarvestDate,
                        x.Farm?.Longitude,
                        x.Farm?.Latitude,
                        x.Farm?.Location,
                        x.Farm?.Town,
                        x.Reference,
                        x.Farm?.NumberOfAcres,
                        x.VarietyId,
                        x.ActualQuantity,
                        x.ExpectedQuantity,
                        Variety = x.Variety.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("admingetactive")]
        public ResultObj AdminGetActive()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.PlantingSeasons.Where(x => x.IsActive).OrderBy(x => x.DatePlanted).Include(x => x.Farm.Farmer).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.IsActive,
                           x.DatePlanted,
                           x.Farm.FarmerId,
                           Farmer = x.Farm.Farmer.Name,
                           x.Farm.Farmer.PhoneNumber,
                           x.FarmId,
                           Farm = x.Farm?.Code,
                           x.ProjectedHarvestDate,
                           x.HarvestDate,
                           x.Farm?.Longitude,
                           x.Farm?.Latitude,
                           x.Farm?.Location,
                           x.Farm?.Town,
                           x.Reference,
                           x.Farm?.NumberOfAcres,
                           x.VarietyId,
                           x.ActualQuantity,
                           x.ExpectedQuantity,
                           Variety = x.Variety.Name

                       }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("admingetbyid")]
        public ResultObj AdminGet(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.PlantingSeasons.Where(x => x.Id == id).Include(x => x.Farm.Farmer).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.IsActive,
                           DatePlanted = x.DatePlanted.ToShortDateString(),
                           x.Farm.FarmerId,
                           Farmer = x.Farm.Farmer.Name,
                           x.Farm.Farmer.PhoneNumber,
                           x.FarmId,
                           Farm = x.Farm?.Code,
                           ProjectedHarvestDate = x.ProjectedHarvestDate.ToShortDateString(),
                           HarvestDate = x.HarvestDate?.ToShortDateString(),
                           x.Farm?.Longitude,
                           x.Farm?.Latitude,
                           x.Farm?.Location,
                           x.Farm?.Town,
                           x.Reference,
                           x.Farm?.NumberOfAcres,
                           x.VarietyId,
                           x.ActualQuantity,
                           x.ExpectedQuantity,
                           Variety = x.Variety.Name
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpDelete]
        [Route("admindelete")]
        public ResultObj AdminDelete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.PlantingSeasons.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                //db.PlantingSeasons.Remove(rec);
                //db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


    }
}