﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Repositories;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("api/public")]
    public class PublicController : ApiController
    {
        [HttpGet]
        [Route("agentstats")]
        public ResultObj GetAgentStats()
        {
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = new AgentDashboardStats
                {
                    ActiveSeasons = db.PlantingSeasons.Count(x => x.CreatedBy == user.UserName && x.IsActive),
                    /*PendingRequests = db.ServiceRequests.Count(
                        x =>
                            x.CreatedBy == user.UserName &&
                            (x.Status == ServiceRequestStatus.Pending || x.Status == ServiceRequestStatus.Accepted ||
                             x.Status == ServiceRequestStatus.Started)),*/
                    RegisteredFarmers = db.Farmers.Count(x => x.CreatedBy == user.UserName && x.IsActive && !x.IsDeleted),
                    RegisteredFarms = db.Farms.Count(x => x.CreatedBy == user.UserName && !x.IsDeleted)
                };

                return WebHelpers.BuildResponse(data, "Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }
        [HttpGet]
        [Route("getregions")]
        public ResultObj GetRegions()
        {
            try
            {
                var data = new BaseRepository<Region>().Get().Select(x => new
                {
                    x.Id,
                    EId =  x.Id,
                    x.Name
                }).ToList();
                return WebHelpers.BuildResponse(data, "Successful", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpGet]
        [Route("getdistricts")]
        public ResultObj GetDistricts(long regionId)
        {
            try
            {
                var data = new BaseRepository<District>().Get().Where(x => x.RegionId == regionId).Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.RegionId
                }).ToList();
                return WebHelpers.BuildResponse(data, "Successful", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpGet]
        [Route("getvarieties")]
        public ResultObj GetVarieties()
        {
            try
            {
                var data = new BaseRepository<Variety>().Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    x.HarvestPeriod,
                    EId = x.Id
                }).ToList();
                return WebHelpers.BuildResponse(data, "Successful", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpGet]
        [Route("getservices")]
        public ResultObj GetServices()
        {
            try
            {
                var data = new BaseRepository<Service>().Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    EId = x.Id
                }).ToList();
                return WebHelpers.BuildResponse(data, "Successful", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }
        [HttpGet]
        [Route("getdistricts")]
        public ResultObj GetDistricts()
        {
            try
            {
                var data = new BaseRepository<District>().Get().Select(x => new
                {
                    x.Id,
                    x.Name,
                    EId = x.Id,
                    x.RegionId,
                    Region = x.Region?.Name,
                }).ToList();
                return WebHelpers.BuildResponse(data, "Successful", true, data.Count);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpPost]
        [Route("pushdata")]
        public ResultObj PushData(OfflineData obj)
        {
            try
            {
                var user = User.Identity.AsAppUser().Result;
                obj.CreatedBy = user.UserName;
                obj.Status = OfflineStatus.Pending;
                obj.TransferedAt = DateTime.Now;
                var db = new AppDbContext();
                db.OfflineData.Add(obj);
                db.SaveChanges();

                return WebHelpers.BuildResponse(obj.Id, "Saved Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }

        [HttpGet]
        [Route("getlasttransferdate")]
        public ResultObj GetDate()
        {
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var res = db.OfflineData.Where(x => x.CreatedBy == user.UserName).OrderByDescending(x => x.TransferedAt).FirstOrDefault()?.TransferedAt;
                if (res == null) res = DateTime.Now.AddYears(-10);
                return WebHelpers.BuildResponse(res, "Successful", true, 1);
            }
            catch (Exception exception)
            {
                return WebHelpers.ProcessException(exception);
            }
        }
    }
}
