﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/processors")]
    public class ProcessorsController : BaseApi<Processor>
    {
        public override ResultObj Post(Processor record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        record.CreatedAt = DateTime.Now;
                        record.ModifiedAt = DateTime.Now;
                        record.CreatedBy = user.UserName;
                        record.ModifiedBy = user.UserName;
                        record.District = null;
                        db.Processors.Add(record);
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        public override ResultObj Put(Processor record)
        {
            using (var db = new AppDbContext())
            {
                using (var transaction = db.Database.BeginTransaction())
                {
                    ResultObj results;
                    try
                    {
                        var user = User.Identity.AsAppUser().Result;
                        var prov = db.Processors.FirstOrDefault(x => x.Id == record.Id);

                        prov.ModifiedAt = DateTime.Now;
                        prov.ModifiedBy = user.UserName;
                        prov.Name = record.Name;
                        prov.PhoneNumber = record.PhoneNumber;
                        prov.Email = record.Email;
                        prov.ContactPerson = record.ContactPerson;
                        prov.Address = record.Address;
                        prov.Town = record.Town;
                        prov.DistrictId = record.DistrictId;
                        prov.Longitude = record.Longitude;
                        prov.Latitude = record.Latitude;
                        prov.GhanaPostGps = record.GhanaPostGps;
                        db.SaveChanges();
                        transaction.Commit();
                        results = WebHelpers.BuildResponse(record.Id, "Saved Successfully.", true, 1);
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        results = WebHelpers.ProcessException(ex);
                    }

                    return results;
                }
            }
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(ProcessorFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Name,
                        x.ContactPerson,
                        x.Reference,
                        x.PhoneNumber,
                        x.Email,
                        x.Address,
                        x.Town,
                        x.DistrictId,
                        District = x.District.Name,
                        Region = x.District.Region.Name,
                        x.Latitude,
                        x.Longitude,
                        x.GhanaPostGps,
                        x.IsDeleted
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("querymappartners")]
        public ResultObj QueryMapPartners(PartnersFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();

                var processorsQuery = db.Processors.Where(x => !x.IsDeleted).Include(x=> x.District.Region);
                if (filter.RegionId > 0) processorsQuery = processorsQuery.Where(x => x.District.RegionId == filter.RegionId);
                if (filter.DistrictId > 0) processorsQuery = processorsQuery.Where(x => x.DistrictId == filter.DistrictId);
                if (!string.IsNullOrEmpty(filter.Name)) processorsQuery = processorsQuery.Where(q => q.Name.ToLower().Contains(filter.Name.ToLower()));
                if (!string.IsNullOrEmpty(filter.Town)) processorsQuery = processorsQuery.Where(q => q.Town.ToLower().Contains(filter.Town.ToLower()));
                if (!string.IsNullOrEmpty(filter.ContactPerson)) processorsQuery = processorsQuery.Where(q => q.ContactPerson.ToLower().Contains(filter.ContactPerson.ToLower()));
                if (!string.IsNullOrEmpty(filter.PhoneNumber)) processorsQuery = processorsQuery.Where(q => q.PhoneNumber.ToLower().Contains(filter.PhoneNumber.ToLower()));
                var records = processorsQuery.ToList().Select(x => new
                {
                    Symbol = "PR",
                    Type = "Processors",
                    x.Name,
                    x.ContactPerson,
                    x.PhoneNumber,
                    x.Email,
                    x.Town,
                    x.Longitude,
                    x.Latitude,
                    x.GhanaPostGps,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                }).ToList();

                var serviceProvidersQuery = db.ServiceProviders.Where(x => !x.IsDeleted).Include(x => x.District.Region);
                if (filter.RegionId > 0) serviceProvidersQuery = serviceProvidersQuery.Where(x => x.District.RegionId == filter.RegionId);
                if (filter.DistrictId > 0) serviceProvidersQuery = serviceProvidersQuery.Where(x => x.DistrictId == filter.DistrictId);
                if (!string.IsNullOrEmpty(filter.Name)) serviceProvidersQuery = serviceProvidersQuery.Where(q => q.Name.ToLower().Contains(filter.Name.ToLower()));
                if (!string.IsNullOrEmpty(filter.Town)) serviceProvidersQuery = serviceProvidersQuery.Where(q => q.Town.ToLower().Contains(filter.Town.ToLower()));
                if (!string.IsNullOrEmpty(filter.ContactPerson)) serviceProvidersQuery = serviceProvidersQuery.Where(q => q.ContactPerson.ToLower().Contains(filter.ContactPerson.ToLower()));
                if (!string.IsNullOrEmpty(filter.PhoneNumber)) serviceProvidersQuery = serviceProvidersQuery.Where(q => q.PhoneNumber.ToLower().Contains(filter.PhoneNumber.ToLower()));

                records.AddRange(serviceProvidersQuery.ToList().Select(x => new
                {
                    Symbol = "SP",
                    Type = "Service Providers",
                    x.Name,
                    x.ContactPerson,
                    x.PhoneNumber,
                    x.Email,
                    x.Town,
                    x.Longitude,
                    x.Latitude,
                    x.GhanaPostGps,
                    District = x.District.Name,
                    Region = x.District.Region.Name
                }).ToList());

                if (!string.IsNullOrEmpty(filter.Type)) records = records.Where(q => q.Type.ToLower().Contains(filter.Type.ToLower())).ToList();

                var total = records.Count();

                if (filter.size > 0) records = records.OrderBy(x => x.Name).Take(filter.size).ToList();
                results = WebHelpers.BuildResponse(records, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }


        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Processors.Include(x => x.District.Region).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                        new
                        {
                            x.CreatedAt,
                            x.CreatedBy,
                            x.ModifiedAt,
                            x.ModifiedBy,
                            x.Id,
                            x.Name,
                            x.ContactPerson,
                            x.Reference,
                            x.PhoneNumber,
                            x.Email,
                            x.Address,
                            x.Town,
                            x.DistrictId,
                            District = x.District.Name,
                            Region = x.District.Region.Name,
                            x.Latitude,
                            x.Longitude,
                            x.GhanaPostGps,
                            x.IsDeleted
                        }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.Processors.Where(x => x.Id == id).Include(x => x.District.Region).OrderBy(x => x.Name).ToList().Select(x =>
                         new
                         {
                             x.CreatedAt,
                             x.CreatedBy,
                             x.ModifiedAt,
                             x.ModifiedBy,
                             x.Id,
                             x.Name,
                             x.ContactPerson,
                             x.Reference,
                             x.PhoneNumber,
                             x.Email,
                             x.Address,
                             x.Town,
                             x.DistrictId,
                             District = x.District.Name,
                             Region = x.District.Region.Name,
                             x.Latitude,
                             x.Longitude,
                             x.GhanaPostGps,
                             x.IsDeleted
                         }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.Processors.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.Processors.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
}