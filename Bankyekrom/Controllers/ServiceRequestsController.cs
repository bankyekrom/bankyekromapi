using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Http;
using Bankyekrom.AxHelpers;
using Bankyekrom.DataAccess.Filters;
using Bankyekrom.DataAccess.Repositories;
using Bankyekrom.Extensions;
using Bankyekrom.Models;

namespace Bankyekrom.Controllers
{
    [Authorize]
    [RoutePrefix("api/servicerequests")]
    public class ServiceRequestsController : BaseApi<ServiceRequest>
    {

        public override ResultObj Post(ServiceRequest record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.CreatedBy = user.UserName;
                record.Farm = null;
                db.ServiceRequests.Add(record);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Put(ServiceRequest record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var rec = db.ServiceRequests.Where(x => x.Id == record.Id).FirstOrDefault();
                if (rec == null) throw new Exception("Update was not successful");
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;
                rec.FarmId = record.FarmId;
                rec.ServiceDate = record.ServiceDate;
                rec.Notes = record.Notes;
                rec.ServiceId = record.ServiceId;
                rec.Charge = record.Charge;
                rec.Status = record.Status;
                rec.AssignedToId = record.AssignedToId;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("query")]
        public ResultObj Query(ServiceRequestsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.CreatedBy == user.UserName).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.FarmId,
                        FarmCode = x.Farm.Code,
                        x.Farm.Latitude,
                        x.Farm.Longitude,
                        x.Farm.Location,
                        x.Farm.GhanaPostGps,
                        x.Farm.Town,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer?.Name,
                        x.Farm.Farmer?.PhoneNumber,
                        Service = x.Service.Name,
                        x.ServiceDate,
                        x.Farm.Farmer.ResidentialAddress,
                        Status = x.Status.ToString(),
                        x.AssignedToId,
                        AssignedTo = x.AssignedTo.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("mobilequery")]
        public ResultObj MobileQuery(ServiceRequestsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter).Where(x => x.CreatedBy == user.UserName).ToList();
                var total = raw.Count();
                var data = raw.OrderByDescending(x => x.ModifiedAt)
                    .Take(filter.Pager.Size * filter.Pager.Page)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.FarmId,
                        FarmCode = x.Farm.Code,
                        x.Farm.Latitude,
                        x.Farm.Longitude,
                        x.Farm.Location,
                        x.Farm.GhanaPostGps,
                        x.Farm.Town,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer?.Name,
                        x.Farm.Farmer?.PhoneNumber,
                        Service = x.Service.Name,
                        x.ServiceDate,
                        x.Farm.Farmer.ResidentialAddress,
                        Status = x.Status.ToString(),
                        x.AssignedToId,
                        AssignedTo = x.AssignedTo.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, total);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get()
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.ServiceRequests.Where(x => x.CreatedBy == user.UserName).Include(x => x.Farm.Farmer).OrderBy(x => x.CreatedAt).ToList().Select(x =>
                        new
                        {
                            x.CreatedAt,
                            x.CreatedBy,
                            x.ModifiedAt,
                            x.ModifiedBy,
                            x.Id,
                            x.Notes,
                            x.FarmId,
                            FarmCode = x.Farm.Code,
                            x.Farm.Latitude,
                            x.Farm.Longitude,
                            x.Farm.Location,
                            x.Farm.GhanaPostGps,
                            x.Farm.Town,
                            x.Farm.FarmerId,
                            Farmer = x.Farm.Farmer?.Name,
                            x.Farm.Farmer?.PhoneNumber,
                            Service = x.Service.Name,
                            x.ServiceDate,
                            x.Farm.Farmer.ResidentialAddress,
                            Status = x.Status.ToString(),
                            x.AssignedToId,
                            AssignedTo = x.AssignedTo.Name
                        }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        public override ResultObj Get(long id)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var db = new AppDbContext();
                var data = db.ServiceRequests.Where(x => x.Id == id).Include(x => x.Farm.Farmer).Include(x => x.Service).Include(x => x.AssignedTo).Include(x => x.Farm.District.Region).OrderBy(x => x.ModifiedAt).ToList().Select(x =>
                       new
                       {
                           x.CreatedAt,
                           x.CreatedBy,
                           x.ModifiedAt,
                           x.ModifiedBy,
                           x.Id,
                           x.Notes,
                           x.FarmId,
                           FarmCode = x.Farm.Code,
                           x.Farm.Latitude,
                           x.Farm.Longitude,
                           x.Farm.Location,
                           x.Farm.GhanaPostGps,
                           x.Farm.Town,
                           x.Farm.FarmerId,
                           Farmer = x.Farm.Farmer?.Name,
                           x.Farm.Farmer?.PhoneNumber,
                           Service = x.Service.Name,
                           x.ServiceId,
                           x.Charge,
                           x.ServiceDate,
                           x.Farm.Farmer.ResidentialAddress,
                           Status = x.Status.ToString(),
                           x.AssignedToId,
                           AssignedTo = x.AssignedTo?.Name,
                           District = x.Farm.District.Name,
                           Region = x.Farm.District.Region.Name
                       }).FirstOrDefault();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
        public override ResultObj Delete(long id)
        {
            ResultObj results;
            try
            {
                results = WebHelpers.BuildResponse(id, "Not Implemented", false, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("cancel")]
        public ResultObj Cancel(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.ServiceRequests.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                rec.Status = ServiceRequestStatus.Cancelled;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Service Request has been Cancelled Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminpost")]
        public ResultObj AdminPost(ServiceRequest record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                record.CreatedAt = DateTime.Now;
                record.ModifiedAt = DateTime.Now;
                record.CreatedBy = user.UserName;
                record.ModifiedBy = user.UserName;
                record.CreatedBy = user.UserName;
                record.Farm = null;
                db.ServiceRequests.Add(record);
                db.SaveChanges();
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Saved Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPut]
        [Route("adminput")]
        public ResultObj AdminPut(ServiceRequest record)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var user = User.Identity.AsAppUser().Result;
                var rec = db.ServiceRequests.Where(x => x.Id == record.Id).FirstOrDefault();
                if (rec == null) throw new Exception("Update was not successful");
                rec.ModifiedAt = DateTime.Now;
                rec.ModifiedBy = user.UserName;
                rec.FarmId = record.FarmId;
                rec.ServiceDate = record.ServiceDate;
                rec.Notes = record.Notes;
                rec.ServiceId = record.ServiceId;
                rec.Charge = record.Charge;
                rec.Status = record.Status;
                rec.AssignedToId = record.AssignedToId;
                db.SaveChanges();
                results = WebHelpers.BuildResponse(record.Id, "Updated Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpPost]
        [Route("adminquery")]
        public ResultObj AdminQuery(ServiceRequestsFilter filter)
        {
            ResultObj results;
            try
            {
                var user = User.Identity.AsAppUser().Result;
                var raw = Repository.Query(filter);
                var data = raw.OrderBy(x => x.ServiceDate).Skip(filter.Pager.Skip())
                    .Take(filter.Pager.Size)
                    .ToList()
                    .Select(x => new
                    {
                        x.CreatedAt,
                        x.CreatedBy,
                        x.ModifiedAt,
                        x.ModifiedBy,
                        x.Id,
                        x.Notes,
                        x.FarmId,
                        FarmCode = x.Farm.Code,
                        x.Farm.Latitude,
                        x.Farm.Longitude,
                        x.Farm.Location,
                        x.Farm.GhanaPostGps,
                        x.Farm.Town,
                        x.Farm.FarmerId,
                        Farmer = x.Farm.Farmer?.Name,
                        x.Farm.Farmer?.PhoneNumber,
                        Service = x.Service.Name,
                        x.ServiceId,
                        x.Charge,
                        x.ServiceDate,
                        x.Farm.Farmer.ResidentialAddress,
                        Status = x.Status.ToString(),
                        x.AssignedToId,
                        AssignedTo = x.AssignedTo?.Name,
                        District = x.Farm.District.Name,
                        Region = x.Farm.District.Region.Name
                    }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, raw.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpGet]
        [Route("adminget")]
        public ResultObj AdminGet()
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var data = db.ServiceRequests.Where(x => x.Id > 0).OrderByDescending(x => x.ServiceDate).ToList().Select(x =>
                         new
                         {
                             x.CreatedAt,
                             x.CreatedBy,
                             x.ModifiedAt,
                             x.ModifiedBy,
                             x.Id,
                             x.Notes,
                             x.FarmId,
                             FarmCode = x.Farm.Code,
                             x.Farm.Latitude,
                             x.Farm.Longitude,
                             x.Farm.Location,
                             x.Farm.GhanaPostGps,
                             x.Farm.Town,
                             x.Farm.FarmerId,
                             Farmer = x.Farm.Farmer?.Name,
                             x.Farm.Farmer?.PhoneNumber,
                             Service = x.Service.Name,
                             x.ServiceDate,
                             x.Farm.Farmer.ResidentialAddress,
                             Status = x.Status.ToString(),
                             x.AssignedToId,
                             AssignedTo = x.AssignedTo.Name,
                             District = x.Farm.District.Name,
                             Region = x.Farm.District.Region.Name
                         }).ToList();
                results = WebHelpers.BuildResponse(data, "Records Loaded", true, data.Count());
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }

        [HttpDelete]
        [Route("admindelete")]
        public ResultObj AdminDelete(long id)
        {
            ResultObj results;
            try
            {
                var db = new AppDbContext();
                var rec = db.ServiceRequests.FirstOrDefault(x => x.Id == id);
                if (rec == null) throw new Exception("Please check the selected record");
                db.ServiceRequests.Remove(rec);
                db.SaveChanges();
                results = WebHelpers.BuildResponse(id, "Deleted Successfully", true, 1);
            }
            catch (Exception ex)
            {
                results = WebHelpers.ProcessException(ex);
            }
            return results;
        }
    }
    }