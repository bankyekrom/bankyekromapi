using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Bankyekrom.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Bankyekrom.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<Models.AppDbContext>
    {
        public UserManager<User> UserManager { get; private set; }

        public Configuration()
            : this(new UserManager<User>(new UserStore<User>(new AppDbContext())))
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        public Configuration(UserManager<User> userManager) { UserManager = userManager; }

        protected override void Seed(AppDbContext context)
        {
            #region Roles [Privileges]
            var roles = new List<IdentityRole>
                        {
                            new IdentityRole {Name = Privileges.CanViewDashboard},
                            new IdentityRole {Name = Privileges.CanViewReports},
                            new IdentityRole {Name = Privileges.CanViewSettings},
                            new IdentityRole {Name = Privileges.CanManageSettings},
                            new IdentityRole {Name = Privileges.CanViewAccounts},
                            new IdentityRole {Name = Privileges.CanManageAccounts},
                            new IdentityRole {Name = Privileges.IsAgent},
                            new IdentityRole {Name = Privileges.IsFarmer},
                            new IdentityRole {Name = Privileges.IsAggregator},
                            new IdentityRole {Name = Privileges.IsProcessor},
                            new IdentityRole {Name = Privileges.IsCustomer},
                            new IdentityRole {Name = Privileges.IsUser},
                            new IdentityRole {Name = Privileges.CanManageFarmers},
                            new IdentityRole {Name = Privileges.CanViewFarmers},
                            new IdentityRole {Name = Privileges.CanManageFarms},
                            new IdentityRole {Name = Privileges.CanViewFarms},
                            new IdentityRole {Name = Privileges.CanViewSeasons},
                            new IdentityRole {Name = Privileges.CanManageSeasons},
                            new IdentityRole {Name = Privileges.CanViewRequests},
                            new IdentityRole {Name = Privileges.CanManageRequests},
                            new IdentityRole {Name = Privileges.CanViewServices},
                            new IdentityRole {Name = Privileges.CanManageServices},
                            new IdentityRole {Name = Privileges.CanViewServiceProviders},
                            new IdentityRole {Name = Privileges.CanManageServiceProviders},
                            new IdentityRole {Name = Privileges.CanManageProcessors},
                            new IdentityRole {Name = Privileges.CanViewProcessors},
                        };

            roles.ForEach(r => context.Roles.AddOrUpdate(q => q.Name, r));
            var a = "";
            roles.ForEach(q => a += q.Name + ",");
            #endregion

            #region App Roles
            var adminProfile = new Profile
            {
                Name = "Administrator",
                Notes = "Administrator Role",
                Privileges = a.Trim(','),
                Locked = true
            };

            var up = Privileges.IsUser;
            var userProfile = new Profile
            {
                Name = "User",
                Notes = "User Role",
                Privileges = up.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x=> x.Name, userProfile);

            var ap = Privileges.IsAgent;
            var agentProfile = new Profile
            {
                Name = "Agent",
                Notes = "Agent Role",
                Privileges = ap.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x => x.Name, agentProfile);

            var fp = Privileges.IsFarmer;
            var farmerProfile = new Profile
            {
                Name = "Farmer",
                Notes = "Farmer Role",
                Privileges = fp.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x => x.Name, farmerProfile);

            var aggp = Privileges.IsAggregator;
            var aggregatorProfile = new Profile
            {
                Name = "Aggregator",
                Notes = "Aggregator Role",
                Privileges = aggp.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x => x.Name, aggregatorProfile);

            var pp = Privileges.IsProcessor;
            var processorProfile = new Profile
            {
                Name = "Processor",
                Notes = "Processor Role",
                Privileges = pp.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x => x.Name, processorProfile);

            var cp = Privileges.IsCustomer;
            var customerProfile = new Profile
            {
                Name = "Customer",
                Notes = "Customer Role",
                Privileges = cp.Trim(','),
                Locked = true
            };
            context.Profiles.AddOrUpdate(x => x.Name, customerProfile);

            #endregion

            #region Users
            var userManager = new UserManager<User>(new UserStore<User>(context))
            {
                UserValidator = new UserValidator<User>(UserManager)
                {
                    AllowOnlyAlphanumericUserNames = false
                }
            };

            //Admin User
            if (UserManager.FindByNameAsync("admin").Result == null)
            {
                var res = userManager.CreateAsync(new User
                {
                    Name = "Administrator",
                    Profile = adminProfile,
                    UserName = "admin",
                    PhoneNumber = "0244112233",
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    TokenVerified = true,
                    Locked = true,
                }, "password");

                if (res.Result.Succeeded)
                {
                    var userId = userManager.FindByNameAsync("admin").Result.Id;
                    roles.ForEach(q => userManager.AddToRole(userId, q.Name));
                }
            }

            if (UserManager.FindByNameAsync("agent1").Result == null)
            {
                var res = userManager.CreateAsync(new User
                {
                    Name = "Francis Miah",
                    Profile = agentProfile,
                    UserName = "agent1",
                    PhoneNumber = "0246903913",
                    CreatedAt = DateTime.Now,
                    ModifiedAt = DateTime.Now,
                    TokenVerified = true,
                    Locked = true,
                }, "password");

                if (res.Result.Succeeded)
                {
                    var userId = userManager.FindByNameAsync("agent1").Result.Id;
                    roles.ForEach(q => userManager.AddToRole(userId, q.Name));
                }
            }

            #endregion

            #region Update Roles
            roles.ForEach(q => context.Roles.AddOrUpdate(q));
            #endregion
            

            context.SaveChanges();
            base.Seed(context);
        }
    }
}
