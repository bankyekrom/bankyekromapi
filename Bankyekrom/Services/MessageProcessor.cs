﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bankyekrom.AxHelpers;
using Bankyekrom.Models;
using Quartz;

namespace Bankyekrom.Services
{
    public class MessageProcessor
    {
        public void Send()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var unsentMsgs = db.MessageOutboxEntries.Where(x => !x.IsSent).ToList();

                    foreach (var msg in unsentMsgs)
                    {
                        MessageHelpers.SendMessage(msg.Id);
                    }
                }
            }
            catch (Exception) { }
        }

        public void DeactivateSeasons()
        {
            try
            {
                using (var db = new AppDbContext())
                {
                    var recs = db.PlantingSeasons.Where(x => x.IsActive).ToList();

                    foreach (var rec in recs)
                    {
                        var now = DateTime.Now;
                        var date = rec.ProjectedHarvestDate.AddMonths(1);
                        if (!rec.HarvestDate.HasValue)
                        {
                            if (date < now)
                                rec.IsActive = false;
                        }
                        else
                        {
                            if(rec.HarvestDate.Value < now)
                                rec.IsActive = false;
                        }
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception) { }
        }
    }
    [DisallowConcurrentExecution]
    public class MessageProcessService : IJob
    {
        public void Execute(IJobExecutionContext context)
        {
            new MessageProcessor().Send();
            new MessageProcessor().DeactivateSeasons();
        }
    }
}