using System;
using System.Data.Entity;
using System.Linq;
using Bankyekrom.Models;

namespace Bankyekrom.DataAccess.Repositories
{
    public class UserRepository : BaseRepository<User>
    {
        public User Get(string username)
        {
            return DbSet.Where(x => x.UserName == username).Include(x => x.Area).SingleOrDefault();
        }

        public override void Update(User entity)
        {
            var theUser = DbSet.Find(entity.Id);
            if (theUser == null) throw new Exception("User not found to update.");
            theUser.Name = entity.Name;
            theUser.Email = entity.Email;
            theUser.ProfileId = entity.ProfileId;
            theUser.PhoneNumber = entity.PhoneNumber;
            theUser.Email = entity.Email;

            SaveChanges();
        }

        public void Delete(string id)
        {
            var user = DbSet.Find(id);
            user.Locked = true;
            user.IsDeleted = true;
            user.ModifiedAt = DateTime.Now;
            SaveChanges();
        }

        public void Restore(string id)
        {
            var user = DbSet.Find(id);
            user.Locked = false;
            user.IsDeleted = false;
            user.ModifiedAt = DateTime.Now;
            SaveChanges();
        }
    }
}