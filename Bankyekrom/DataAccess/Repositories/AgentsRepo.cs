﻿using Bankyekrom.AxHelpers;
using Bankyekrom.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bankyekrom.DataAccess.Repositories
{
    public class AgentsRepo
    {
        public void SaveOrUpdateFarmers(List<Farmer> recs, AppDbContext db, User user)
        {
            foreach(var rec in recs)
            {
                var exst = db.Farmers.Where(x => x.Id == rec.Id).FirstOrDefault();
                if(exst != null)
                {
                    exst.EId = exst.Id;
                    exst.ModifiedAt = DateTime.Now;
                    exst.ModifiedBy = user.UserName;
                    exst.Name = rec.Name;
                    exst.PhoneNumber = rec.PhoneNumber;
                    exst.DateOfBirth = rec.DateOfBirth;
                    exst.ResidentialAddress = rec.ResidentialAddress;
                    exst.Town = rec.Town;
                    exst.AreaId = user.AreaId.Value;
                    exst.IsDeleted = rec.IsDeleted;
                    exst.Gender = rec.Gender;
                    exst.GhanaPostGps = rec.GhanaPostGps;
                }
                else
                {
                    rec.ModifiedAt = DateTime.Now;
                    rec.ModifiedBy = user.UserName;
                    rec.CreatedBy = user.UserName;
                    rec.AreaId = user.AreaId.Value;
                    rec.Reference = MessageHelpers.GenerateRandomNumber(8);
                    db.Farmers.Add(rec);
                }
                db.SaveChanges();
            }
        }

        public void SaveOrUpdateFarms(List<Farm> recs, AppDbContext db, User user)
        {
            foreach (var rec in recs)
            {
                var farmer = db.Farmers.FirstOrDefault(x => x.Id == rec.FarmerId || x.EId == rec.FarmerId);
                if (farmer == null) throw new Exception("Farmer could not be found");
                var exst = db.Farms.Where(x => x.Id == rec.Id).FirstOrDefault();
                if (exst != null)
                {
                    exst.EId = exst.Id;
                    exst.ModifiedAt = DateTime.Now;
                    exst.ModifiedBy = user.UserName;
                    exst.Code = rec.Code;
                    exst.FarmerId = farmer.Id;
                    exst.Town = rec.Town;
                    exst.Location = rec.Location;
                    exst.NumberOfAcres = rec.NumberOfAcres;
                    exst.Latitude = rec.Latitude;
                    exst.Longitude = rec.Longitude;
                    exst.DistrictId = rec.DistrictId;
                    exst.GhanaPostGps = rec.GhanaPostGps;
                    exst.IsDeleted = rec.IsDeleted;
                    exst.GhanaPostGps = rec.GhanaPostGps;
                }
                else
                {
                    rec.ModifiedAt = DateTime.Now;
                    rec.ModifiedBy = user.UserName;
                    rec.CreatedBy = user.UserName;
                    rec.FarmerId = farmer.Id;
                    rec.Reference = MessageHelpers.GenerateRandomNumber(8);
                    db.Farms.Add(rec);
                }
                db.SaveChanges();
            }
        }

        public void SaveOrUpdateRequests(List<ServiceRequest> recs, AppDbContext db, User user)
        {
            foreach (var rec in recs)
            {
                var service = db.SeasonServices.FirstOrDefault(x => x.Id == rec.ServiceId);
                if (service == null) throw new Exception("Service could not be found");
                var farm = db.Farms.FirstOrDefault(x => x.Id == rec.FarmId || x.EId == rec.FarmId);
                if (farm == null) throw new Exception("Farm could not be found");
                var exst = db.ServiceRequests.Where(x => x.Id == rec.Id).FirstOrDefault();
                if (exst != null)
                {
                    exst.EId = exst.Id;
                    exst.ModifiedAt = DateTime.Now;
                    exst.ModifiedBy = user.UserName;
                    exst.Notes = rec.Notes;
                    exst.FarmId = farm.Id;
                    exst.ServiceDate = rec.ServiceDate;
                    //exst.Charge = rec.Charge;
                    //exst.AssignedToId = rec.AssignedToId;
                    exst.ServiceId = service.Id;
                }
                else
                {
                    rec.ModifiedAt = DateTime.Now;
                    rec.ModifiedBy = user.UserName;
                    rec.CreatedBy = user.UserName;
                    rec.FarmId = farm.Id;
                    rec.ServiceId = service.Id;
                    rec.Status = ServiceRequestStatus.Pending;
                    db.ServiceRequests.Add(rec);
                }
                db.SaveChanges();
            }
        }

        public void SaveOrUpdateSeasons(List<PlantingSeason> recs, AppDbContext db, User user)
        {
            foreach (var rec in recs)
            {
                var variety = db.Varieties.FirstOrDefault(x => x.Id == rec.VarietyId);
                if (variety == null) throw new Exception("Variety could not be found");
                var farm = db.Farms.FirstOrDefault(x => x.Id == rec.FarmId || x.EId == rec.FarmId);
                if (farm == null) throw new Exception("Farm could not be found");
                var exst = db.PlantingSeasons.Where(x => x.Id == rec.Id).FirstOrDefault();
                if (exst != null)
                {
                    exst.EId = exst.Id;
                    exst.ModifiedAt = DateTime.Now;
                    exst.ModifiedBy = user.UserName;
                    exst.Notes = rec.Notes;
                    exst.FarmId = farm.Id;
                    exst.IsActive = rec.IsActive;
                    exst.DatePlanted = rec.DatePlanted;
                    exst.ProjectedHarvestDate = rec.DatePlanted.AddMonths(variety.HarvestPeriod);
                    exst.VarietyId = variety.Id;
                    exst.ExpectedQuantity = 0;
                }
                else
                {
                    rec.ModifiedAt = DateTime.Now;
                    rec.ModifiedBy = user.UserName;
                    rec.CreatedBy = user.UserName;
                    rec.FarmId = farm.Id;
                    rec.VarietyId = variety.Id;
                    rec.ProjectedHarvestDate = rec.DatePlanted.AddMonths(variety.HarvestPeriod);
                    rec.ExpectedQuantity = 0;
                    rec.IsActive = true;
                    rec.Reference = MessageHelpers.GenerateRandomNumber(8);
                    db.PlantingSeasons.Add(rec);
                }
                db.SaveChanges();
            }
        }
    }
}