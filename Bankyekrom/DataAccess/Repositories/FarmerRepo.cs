﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Bankyekrom.Models;

namespace Bankyekrom.DataAccess.Repositories
{
    public class FarmerRepo: BaseRepository<Farmer>
    {

    }

    public class FarmRepo : BaseRepository<Farm>
    {
        public List<Farm> GetByFarmerId(long farmerId)
        {
            return DbContext.Farms.Where(x => x.FarmerId == farmerId).ToList();
        }
    }

    public class PlantingSeasonRepo : BaseRepository<PlantingSeason>
    {
    }

    public class ServiceRequestRepo : BaseRepository<ServiceRequest>
    {
        public void CancelRequest(long requestId)
        {
            var req = DbContext.ServiceRequests.FirstOrDefault(x => x.Id == requestId);
            req.Status = ServiceRequestStatus.Cancelled;
            req.ModifiedAt = DateTime.Now;
            DbContext.SaveChanges();
        }
    }
}