using System.Data.Entity;
using System.Linq;
using Bankyekrom.Models;

namespace Bankyekrom.DataAccess.Filters
{
    public class UserFilter : Filter<User>
    {
        public long ProfileId;

        public override IQueryable<User> BuildQuery(IQueryable<User> query)
        {
            if (ProfileId > 0) query = query.Where(q => q.Profile.Id == ProfileId);

            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class ServiceRequestsFilter : Filter<ServiceRequest>
    {
        public long Id;
        public long FarmId;
        public long FarmerId;
        public long ServiceId;
        public ServiceRequestStatus? Status;

        public override IQueryable<ServiceRequest> BuildQuery(IQueryable<ServiceRequest> query)
        {
            query = query.Include(x=> x.Farm.Farmer).Include(x=> x.Service).Include(x=> x.AssignedTo).Include(x => x.Farm.District.Region);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (FarmId > 0) query = query.Where(q => q.FarmId == FarmId);
            if (FarmerId > 0) query = query.Where(q => q.Farm.FarmerId == FarmerId);
            if (ServiceId > 0) query = query.Where(q => q.ServiceId == ServiceId);

            return query;
        }
    }

    public class FarmerFilter : Filter<Farmer>
    {
        public long Id;
        public long AreaId;
        public long RegionId;
        public long DistrictId;
        public string Name;
        public string Town;
        public string CreatedBy;

        public override IQueryable<Farmer> BuildQuery(IQueryable<Farmer> query)
        {
            query = query.Include(x => x.Area.District.Region).Include(x=> x.Farms);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (AreaId > 0) query = query.Where(q => q.AreaId == AreaId);
            if (RegionId > 0) query = query.Where(q => q.Area.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(q => q.Area.DistrictId == DistrictId);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.Town.ToLower().Contains(Town.ToLower()));
            if (!string.IsNullOrEmpty(CreatedBy)) query = query.Where(q => q.CreatedBy.ToLower().Contains(CreatedBy.ToLower()));
            query = query.Where(q => !q.Hidden && q.IsActive && !q.IsDeleted);
            return query;
        }
    }

    public class FarmsFilter : Filter<Farm>
    {
        public long Id;
        public long AreaId;
        public long RegionId;
        public long DistrictId;
        public string Code;
        public string Town;
        public string CreatedBy;
        public int Size;

        public override IQueryable<Farm> BuildQuery(IQueryable<Farm> query)
        {
            query = query.Include(x => x.Farmer.Area).Include(x=> x.District.Region);
            if (AreaId > 0) query = query.Where(q => q.Farmer.AreaId == AreaId);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (RegionId > 0) query = query.Where(q => q.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(q => q.DistrictId == DistrictId);
            if (!string.IsNullOrEmpty(Code)) query = query.Where(q => q.Code.ToLower().Contains(Code.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.Town.ToLower().Contains(Town.ToLower()));
            if (!string.IsNullOrEmpty(CreatedBy)) query = query.Where(q => q.CreatedBy.ToLower().Contains(CreatedBy.ToLower()));
            query = query.Where(q => !q.Hidden && !q.IsDeleted);
            return query;
        }
    }

    public class SeasonFilter : Filter<PlantingSeason>
    {
        public long Id;
        public long RegionId;
        public long DistrictId;
        public long FarmerId;
        public long FarmId;
        public string Name;
        public string Town;
        public string CreatedBy;
        public long VarietyId;
        public string FarmCode;

        public override IQueryable<PlantingSeason> BuildQuery(IQueryable<PlantingSeason> query)
        {
            query = query.Include(x => x.Farm.Farmer.Area.District.Region).Include(x => x.Farm.District.Region).Include(x=> x.Variety);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (RegionId > 0) query = query.Where(q => q.Farm.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(q => q.Farm.DistrictId == DistrictId);
            if (FarmerId > 0) query = query.Where(q => q.Farm.FarmerId == FarmerId);
            if (FarmId > 0) query = query.Where(q => q.FarmId == FarmId);
            if (VarietyId > 0) query = query.Where(q => q.VarietyId == VarietyId);
            if (!string.IsNullOrEmpty(CreatedBy)) query = query.Where(q => q.CreatedBy.ToLower().Contains(CreatedBy.ToLower()));
            if (!string.IsNullOrEmpty(FarmCode)) query = query.Where(q => q.Farm.Code.ToLower().Contains(FarmCode.ToLower()));
            query = query.Where(q => !q.Hidden);
            return query;
        }
    }

    public class SeasonMapFilter
    {
        public int Size;
        public long RegionId;
        public long DistrictId;
        public long FarmerId;
        public long FarmId;
        public long VarietyId;
        public string FarmCode;
    }

    public class ServiceProviderFilter : Filter<ServiceProvider>
    {
        public long Id;
        public long RegionId;
        public long DistrictId;
        public string Name;
        public string Town;
        public string ContactPerson;

        public override IQueryable<ServiceProvider> BuildQuery(IQueryable<ServiceProvider> query)
        {
            query = query.Include(x => x.District.Region).Include(x => x.Services);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (RegionId > 0) query = query.Where(q => q.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(q => q.DistrictId == DistrictId);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.Town.ToLower().Contains(Town.ToLower()));
            if (!string.IsNullOrEmpty(ContactPerson)) query = query.Where(q => q.ContactPerson.ToLower().Contains(ContactPerson.ToLower()));
            query = query.Where(q => !q.Hidden && q.IsActive && !q.IsDeleted);
            return query;
        }
    }

    public class ProcessorFilter : Filter<Processor>
    {
        public long Id;
        public long RegionId;
        public long DistrictId;
        public string Name;
        public string Town;
        public string ContactPerson;

        public override IQueryable<Processor> BuildQuery(IQueryable<Processor> query)
        {
            query = query.Include(x => x.District.Region);
            if (Id > 0) query = query.Where(q => q.Id == Id);
            if (RegionId > 0) query = query.Where(q => q.District.RegionId == RegionId);
            if (DistrictId > 0) query = query.Where(q => q.DistrictId == DistrictId);
            if (!string.IsNullOrEmpty(Name)) query = query.Where(q => q.Name.ToLower().Contains(Name.ToLower()));
            if (!string.IsNullOrEmpty(Town)) query = query.Where(q => q.Town.ToLower().Contains(Town.ToLower()));
            if (!string.IsNullOrEmpty(ContactPerson)) query = query.Where(q => q.ContactPerson.ToLower().Contains(ContactPerson.ToLower()));
            query = query.Where(q => !q.Hidden && !q.IsDeleted);
            return query;
        }
    }

    public class PartnersFilter
    {
        public long RegionId;
        public long DistrictId;
        public string Name;
        public string Town;
        public string ContactPerson;
        public string PhoneNumber;
        public string Type;
        public int size;

    }

}