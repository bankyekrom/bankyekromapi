﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using Bankyekrom.Models;
using RestSharp;
using RestSharp.Authenticators;

namespace Bankyekrom.AxHelpers
{
    public class MessageHelpers
    {
        private static readonly MessageService MsgServiceSettings = SetupConfig.Setting.MessageService;
        public static void SendMessage(long msgId)
        {
            if (!MsgServiceSettings.IsActive) return;
            using (var db = new AppDbContext())
            {
                var eoe = db.MessageOutboxEntries.First(x => x.Id == msgId && !x.IsSent);

                var client = new RestClient(MsgServiceSettings.BaseUrl);
                var request = new RestRequest(MsgServiceSettings.SendMessageUrl, Method.POST)
                {
                    RequestFormat = DataFormat.Json
                };
                request.AddHeader(HttpRequestHeader.Authorization.ToString(),
                    $"Bearer {MsgServiceSettings.ApiKey}");
                request.AddParameter("Type", MessageType.SMS.ToString());
                request.AddParameter("SenderId", eoe.Sender);
                request.AddParameter("Subject", eoe.Subject);
                request.AddParameter("Message", eoe.Message);
                request.AddParameter("Recipients", eoe.Receiver);
                //request.AddBody(eoe.Message);
                var res = client.Execute(request);
                if (res.StatusCode != HttpStatusCode.OK) return;
                eoe.IsSent = true;
                eoe.LastAttemptMessage = res.ResponseStatus.ToString();
                eoe.LastAttemptDate = DateTime.Now;
                db.SaveChanges();
            }
        }

        public static Farmer GetFarmerByUsername(string username)
        {
            var db = new AppDbContext();
            var farmer = db.Farmers.First(x => x.CreatedBy == username);
            return farmer;
        }

        public static string GenerateRandomString(int length)
        {
            var stringBuilder = new StringBuilder(length);
            var chArray = "abcdefghijklmnopqrstuvwxyz0123456789".ToCharArray();
            var random = new Random((int)DateTime.Now.Ticks);
            for (var index = 0; index < length; ++index)
                stringBuilder.Append(chArray[random.Next(chArray.Length)]);
            return stringBuilder.ToString().ToUpper();
        }

        public static string GenerateRandomNumber(int length)
        {
            var stringBuilder = new StringBuilder(length);
            var chArray = "0123456789".ToCharArray();
            var random = new Random((int)DateTime.Now.Ticks);
            for (var index = 0; index < length; ++index)
                stringBuilder.Append(chArray[random.Next(chArray.Length)]);
            return stringBuilder.ToString();
        }
    }
}